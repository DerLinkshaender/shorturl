
# ShortURL -- Kurz-URLs in Go(lang)

*First off, why is this proejct in German? This the companion project to a small series of blog 
posts about the how and why of shortening URLs. As my blog is written in German, so is this project
to allow people to follow along. If you have any questions, but do not speak enough German, feel free
to contact.*

**Hinweis** das Repo ist noch nicht "fertig", erst mit der Veröffentlichung des dritten Teil der Serie Ende Dezember.
Die jetzt hier vorhandenen Informationen sollten für Interessierte, die vorab basteln wollen, aber ausreichen.

## Hintergrund des Projekts

* [Teil 1 der Reihe](https://www.arminhanisch.de/2018/12/shortlinks-selbstgemacht/)
* [Teil 2 der Reihe](https://www.arminhanisch.de/2018/12/shortlinks-teil2/)
* [Teil 3 der Reihe](https://www.arminhanisch.de/2019/01/shortlinks-teil3/)

## Schnellstart

Zuerst das Repository klonen oder die Dateien herunterladen und in einem Verzeichnis auspacken, dann mit dem Compiler per `go build` bauen lassen.

### Kompiliertes Binary runterladen

**Hinweis:** ich übernehme keinerlei Garantie für die Funktion bzw. Nutzbarkeit der binären Dateien auf irgeneinem System. Der Einsatz erfolgt auf eigenes Risiko, diese Binaries sind als Service für Leute gedacht, die keinen Go-Compiler installiert haben.
Vergleichen Sie nach dem Download auf jeden Fall die SHA256-Checksumme anhand der folgenden Liste! Kompiliert wurde mit `go1.11.4 darwin/amd64`

* bd1d4f2c18a4c75d6717e8f0431909da1633c38cc4cfe2bb9820a75e29db75c9  shorturl_386.exe
* 82ac32f5ca04967d87f3192b024ab337647417332eb5f9c6a6c199ee8801b9ca  shorturl_amd64.exe
* 75f4df3bd36f8bbbc588e60cb3a00860586555ba7cbb40659bc62aef09f3da1d  shorturl_linux_386
* 56141adcbdb30e8bfead04cd711b98e494f9988380685a73c14044d7284cbfc6  shorturl_linux_amd64
* 33ed11f186e96902b2e019d3e2d9e559b6e92f33cd663006be335374d0913d80  shorturl_linux_arm5
* 5830c01eb5bb553dcb94108797d02dec7c0c368eb2cb489418dbd8e99a2af76e  shorturl_osx


### Erstmal lokal ausprobieren?

Ich habe das Tool für die Kurz-URLs so gebaut, dass es sich auch lokal auf Ihrem Rechner ohne Webserver ausprobieren lässt. Ob `shorturl` einen eigenen HTTP-handler mitbringt oder als FastCGI läuft, wird in der Konfigurationsdatei `shorturl.toml` definiert.
Öffnen Sie diese mit einem Texteditor (**Text**, nicht Word oder Wordpad! Sondern einen Programmier-Editor wie Notepad++, SublimeText, Atom, etc.). Diese sollte folgenden Inhalt haben (falls nicht, bitte entsprechend anpassen). Dann die Datei wieder speichern und schließen.

	User = "admin"
	Pass = "PASSWORD"
	LocalServer = "127.0.0.1:8080"
	StoreFile = "./shorturl.db"

Jetzt können Sie die `shorturl.fcgi` als ausführbares Programm starten. Sollten Sie Windows benutzen, müssen Sie erst die Dateierweiterung von `.fcgi` auf `.exe` ändern. Unter MacOS oder Linux einfach das `x`-Bit zum Ausführen per `chmod` setzen (`chmod +x ./shorturl.fcgi`)

Auf der Konsole sehen Sie dann eine Ausgabe wie diese hier:

	2018/12/03 22:17:22 Running with local server at 127.0.0.1:8080

### Auf dem Webserver installieren

Ich gehe davon aus, dass ein Apache 2.x benutzt wird und das Modul `mod_rewrite` aktiv ist.

 * Die Datei in `shorturl.fcgi` umbenennen (*falls nicht per `-o` beim Compile geschehen*)
 * Die Datei in das FastCGI-Verzeichnis des Webservers kopieren (üblich ist `fcgi-bin`)
 * Die Datei `shorturl.toml` ebenfalls da hin kopiern und anpassen
   * `User = "admin"` -- der Benutzername für das Eintragen und Löschen
   * `Pass = "PASSWORD"` -- das Kennwort für das Eintragen und Löschen
   * `LocalServer = ""` -- Benötigen Sie nur zur lokalen Ausführung, hier also den Leerstring eintragen.
   * `StoreFile = "/pfad/zur/datei/shorturl.db"` -- Siehe oben. Direkt das `/fcgi-bin` sollte es nach Möglichkeit auch nicht sein aus Sicherheitsgründen. *Ich habe mir unterhalb des HTML-Basisordner (bei mir `/var/www/html`) einfach einen Ordner `storage` erzeugt und da liegt die Datei*.
 * Die Datei `.htaccess` bearbeiten, um das Tool einzutragen (siehe den nächsten Abschnitt)

### Die .htaccess bearbeiten

Im Hauptverzeichnis Ihrers Webauftritts legen Sie (wenn es die noch nicht gibt) eine Datei `.htaccess` an und tragen folgenden Inhalt ein.

	<IfModule mod_fcgid.c>
	   AddHandler fcgid-script .fcgi
	   <Files ~ (\.fcgi)>
	       SetHandler fcgid-script
	       Options +ExecCGI
	   </Files>
	</IfModule>
	
	<IfModule mod_rewrite.c>
	   RewriteEngine On
	   RewriteBase /
	   RewriteCond %{REQUEST_FILENAME} !-f
	   RewriteRule ^(.*)$ /fcgi-bin/shorturl.fcgi/$1 [QSA,L]
	</IfModule>

## Ausprobieren ...

Jetzt können Sie das Tool aufrufen. Da ich nicht weiß, welche kurze Domain Sie sich registiert haben, verwende ich hier für die Beispiele die Domain "`doma.in`".

### Eine neue URL eintragen

Da wir noch keine Kurz-URL haben, ist dies ein guter Startpunkt. Dazu rufen Sie den Endpunkt  `/admin/add` auf, also in unserem Beispiel `http://doma.in/admin/add`. Zuerst sollte eine Dialogbox erscheinen, in der Sie nach dem Benutzernamen und dem Passwort (siehe oben) gefragt werden, um eine neue URL einzutragen. 

Geben Sie in das erste Eingabefeld die lange URL ein (das zweite mit der optionalen Wunsch-URL lassen wir leer) und klicken Sie dann auf die Schaltfläche `[Senden]`.

Sie sehen jetzt den vergeben Schlüssel (im Beispiel oben `by5I8QrVXia`) und das Ziel des Links.

Wenn Sie im Eingabefeld "Optionale Wunsch-URL" einen eigenen Wert eingeben (z.B. `shorty`) wird dieser auf Gültigkeit geprüft (muss den URL-Konventionen entsprechen und darf noch nicht existieren) und dann statt des automatisch erzeugten Hashwerts verwendet.

### Bestehende Kurz-URLs auflisten

Dazu verwenden Sie den Endpunkt `/admin/list`, also in unserem Beispiel `http://doma.in/admin/list`. 
Sie sehen jetzt alle definierten Kurz-URLs und ihr Ziel, dass Sie auf Wunsch auch gleich anklicken können.

##### Bonus-Material

Beim Testen wollte ich die Liste der Kurz-URLs und ihrer Ziele nicht in HTML, sondern als CSV-Ausgabe. *Ja, Pustekuchen, haste ja nicht programmiert, Alter!* :weary: -- *Doch, jetzt schon* :thumbsup:<br>
Sie können an die URL `/admin/list` jetzt einfach den Parameter `?format=` anhängen und dabei entweder `csv`, `json` oder `xml` verwenden. Beispiel: `http://doma.in/admin/list?format=json` und schon bekommen Sie die Ausgabe in einem maschinenlesbaren Format! Gerade bei längeren Sammlungen ist dies eine schnelle Export-Methode.

### Eine bestehende Kurz-URL löschen

Dazu verwenden Sie den Endpunkt `/admin/del/KEY`, also in unserem Beispiel `http://doma.in/admin/del/by5I8QrVXia`. Der zu löschende Schlüssel ist also Bestandteil der URL. Waren Sie noch nicht angemeldet, wird auch hier wieder zuerst Benutzername und Passwort verlangt (wir wollen ja nicht jeden unsere Kurzlinks löschen lassen!). 

### Einen Kurzlink aufrufen

Dies ist der einfachste Fall: der Schlüssel wird als Pfad hinter unserem kurzen Domain-Namen verwendet. 
In unserem Fall (siehe die Liste der definierten URLs oben): `http://doma.in/shorty`. Danach landen Sie direkt auf dem Ziel des Links.

### Komfortfunktionen

Ah, Komfortfunktionen... :smirk: <br>
Beschrieben hatte ich die im zweiten Teil der Blogpost-Serie. Sie machen den Umgang mit Kurz-URLs etwas leichter. Ich verwende für die folgenden Beispiel den Kurzcode `shorty` aus dem Beispiel oben.

#### Zielanzeige

Um das Ziel eines Kurz-Links anzuzeigen und nicht automatisch weiter zuleiten, hängen Sie einfach ein **`.d`** an den Kurzcode an (für *destination*), z.B. `http://doma.in/short.d`
 

#### Informationen über das Ziel

Um Informationen über das Ziel eines Kurz-Links anzuzeigen (den sogenannten **HTTP-Header**), hängen Sie einfach ein **`.h`** an den Kurzcode an (für *header*), z.B. `http://doma.in/short.h`
 

#### Einen QR-Code erzeugen

Wenn Sie für das Ziel der Kurz-URL einen QR-Code benötigen, dann hängen Sie einfach ein **`.q`** an den Kurzcode an (für *QR-Code*)


Natürlich enthält der QR-Code gleich die URL des Ziel (alles andere wäre auch Unsinn), so dass für die Verteilung des QR-Codes unerheblich ist, ob das Tool in Betrieb ist oder nicht. Als Nebeneffekt haben Sie nun einen eigenen, privaten QR-Code Generator für Links, wenn Sie die eingetragene URL nach dem Anzeigen und Speichern des QR-Codes wieder austragen...

### Technische Details: die Ablage der Link-Daten

Digitale Mündigkeit bedeutet auch die Herrschaft über die eigenen Daten. Sie sollen auch ohne dieses Tool in der Lage sein, die Daten anderweitig zu verwenden oder beim Einsatz eines anderen Tools umziehen zu können. Dazu plane ich als Ergänzung noch ein kleines Zusatzwerkzeug, dass aus der Datenbank mit den Kurzlinks eine CSV- oder Exceldatei erzeugt. Falls Sie solange nicht warten wollen oder einfach wissen möchten, wie die Datenstrukturen innerhalb der Datei aussehen, finden Sie hier weitere Informationen.

Das Projekt nutzt einen sog. "Key-Value-Store", also eine einfach Ablage von Werten unter einem Zeichenketten-Schlüssel. Dazu verwende ich das Projekt **BoltDB**: https://github.com/boltdb/bolt 

Die darunterliegenden Datenstrukturen werden nach der `gob`-Konvention kodiert. Details dazu finden Sie hier: https://golang.org/pkg/encoding/gob/

